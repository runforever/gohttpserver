<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Golang 编写的简单HTTP服务器</a>
<ul>
<li><a href="#sec-1-1">1.1. 设计文档</a></li>
<li><a href="#sec-1-2">1.2. 特点</a></li>
<li><a href="#sec-1-3">1.3. 安装</a></li>
<li><a href="#sec-1-4">1.4. 使用</a>
<ul>
<li><a href="#sec-1-4-1">1.4.1. 自定义www目录</a></li>
<li><a href="#sec-1-4-2">1.4.2. 自定义配置文件</a></li>
</ul>
</li>
<li><a href="#sec-1-5">1.5. 配置文件格式</a></li>
</ul>
</li>
</ul>
</div>
</div>


# Golang 编写的简单HTTP服务器

## 设计文档

参考<http://runforever.github.io/>

## 特点

1.  高并发
2.  支持html、css、js、jpeg、png
3.  支持的状态码 200， 304， 404
4.  支持配置config.ini
5.  简单，高效

## 安装

1.  git clone <https://runforever@bitbucket.org/runforever/gohttpserver.git>
2.  安装依赖，运行./deps
3.  编译./install

## 使用

编译后生成bin目录   
运行 bin/gohttpserver可以打开服务器   

### 自定义www目录

在bin目录里新建www目录，里面需要放置index.html

### 自定义配置文件

在bin目录里新建config.ini文件

## 配置文件格式

    [host]
        ; IP 地址配置
        ip = 127.0.0.1
    
        ; 端口号配置
        port = 8080
    
    [static]
        ; 静态文件目录配置
        dir = /yourdir/www/
    
    [logging]
        ; 输出日志级别DEBUG，INFO，WARNING， ERROR， NOTICE，CRETICAL
        loglevel = DEBUG
    
    [advance]
        ; socket 超时配置
        timeout = 11
    
        ; request 请求channel 容量
        reqchancap = 20
