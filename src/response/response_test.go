/*
* response unit test
*
 */

package response

import (
	"config"
	"fmt"
	"os"
	"path/filepath"
	"request"
	"testing"
)

func TestGetStatus(t *testing.T) {
	staticDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	config.SerConfig.StaticDir = staticDir
	fmt.Println(staticDir)

	filename := staticDir + "index.html"
	file, _ := os.Create(filename)

	file.WriteString("<h1>hello world</h1>")
	file.Close()

	defer file.Close()

	// test 200 0k
	request200 := &request.Request{
		Method:         "GET",
		PathInfo:       "/",
		Protocol:       "Http/1.1",
		Host:           "127.0.0.1:8080",
		Accept:         "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		AcceptEncoding: "gzip,deflate,sdch",
		AcceptLanguage: "zh-CN,zh;q=0.8,en;q=0.6,hr;q=0.4,zh-TW;q=0.2",
		Connection:     "keep-alive",
		UserAgent:      "t Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36",
	}

	res200 := GoResponse(request200)
	resMeta200 := res200.getStatusMeta()

	if resMeta200.StatusCode != "200 OK" {
		t.Error("http status code not equal 200 code is ", resMeta200.StatusCode)
	}

	// test 304 not modified
	f, _ := os.Stat(filename)
	modifiedTimeStr := getGMTTimeStr(f.ModTime())

	request304 := &request.Request{
		Method:          "GET",
		PathInfo:        "/",
		Protocol:        "Http/1.1",
		Host:            "127.0.0.1:8080",
		Accept:          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		AcceptEncoding:  "gzip,deflate,sdch",
		AcceptLanguage:  "zh-CN,zh;q=0.8,en;q=0.6,hr;q=0.4,zh-TW;q=0.2",
		Connection:      "keep-alive",
		IfModifiedSince: modifiedTimeStr,
		UserAgent:       "t Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36",
	}

	res304 := GoResponse(request304)
	resMeta304 := res304.getStatusMeta()

	if resMeta304.StatusCode != "304 Not Modified" {
		t.Error("http status code not equal 304 code is ", resMeta304.StatusCode)
	}
}
