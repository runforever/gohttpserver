/*
* request unit test
*
 */

package request

import "testing"

func TestParseReq(t *testing.T) {
	requestSlice := []string{
		"GET / HTTP/1.1",
		"Host: 127.0.0.1:8080",
		"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Encoding: gzip,deflate,sdch",
		"Accept-Language: zh-CN,zh;q=0.8,en;q=0.6,hr;q=0.4,zh-TW;q=0.2",
		"Cache-Control: max-age=0",
		"Connection: keep-alive",
		"Cookie: messages=helloworld",
		"If-Modified-Since:Mon, 26 May 2014 23:55:03 GMT",
		"User-Agent:t Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36",
	}
	req := parseRequest(requestSlice)
	if req.Method != "GET" {
		t.Error("parse method Error, error field", req.Method)
	}
	if req.PathInfo != "/" {
		t.Error("parse path Error, error field", req.PathInfo)
	}
	if req.Protocol != "HTTP/1.1" {
		t.Error("parse Protocol Error, error field", req.Protocol)
	}
	if req.Connection != "keep-alive" {
		t.Error("parse Connection Error, error field", req.Connection)
	}
	if req.IfModifiedSince != "Mon, 26 May 2014 23:55:03 GMT" {
		t.Error("parse Connection Error, error field", req.IfModifiedSince)
	}
	if req.Cookie["messages"] != "helloworld" {
		t.Error("parse Cookie Error, error field", req.Cookie)
	}
}
