/*
*  parse http request
*
 */
package request

import (
	"bufio"
	"net"
	"reflect"
	"strings"

	"config"
	"logger"
)

type reqProcMeta struct {
	Proc  func(*Request, string, string, string)
	Field string
}

var parseMeta = map[string]*reqProcMeta{
	"GET": &reqProcMeta{
		Proc:  parseHttpInfo,
		Field: "GET",
	},
	"Host": &reqProcMeta{
		Proc:  parseAdditionInfo,
		Field: "Host",
	},
	"Connection": &reqProcMeta{
		Proc:  parseAdditionInfo,
		Field: "Connection",
	},
	"Accept": &reqProcMeta{
		Proc:  parseAdditionInfo,
		Field: "Accept",
	},
	"User-Agent": &reqProcMeta{
		Proc:  parseAdditionInfo,
		Field: "UserAgent",
	},
	"Accept-Encoding": &reqProcMeta{
		Proc:  parseAdditionInfo,
		Field: "AcceptEncoding",
	},
	"Accept-Language": &reqProcMeta{
		Proc:  parseAdditionInfo,
		Field: "AcceptLanguage",
	},
	"If-Modified-Since": &reqProcMeta{
		Proc:  parseAdditionInfo,
		Field: "IfModifiedSince",
	},
	"Cookie": &reqProcMeta{
		Proc:  parseCookie,
		Field: "Cookie",
	},
}

// Stuct request
type Request struct {
	PathInfo        string
	Method          string
	Protocol        string
	Host            string
	Accept          string
	Connection      string
	UserAgent       string
	AcceptEncoding  string
	AcceptLanguage  string
	IfModifiedSince string
	Cookie          map[string]string
}

// init http method, http path, http protocol
func parseHttpInfo(r *Request, prefix, field, line string) {
	splitLine := strings.Split(line, " ")
	r.Method = splitLine[0]
	r.PathInfo = splitLine[1]
	r.Protocol = splitLine[2]
}

// init http addition info, user agent, accept etc.
func parseAdditionInfo(r *Request, prefix, field, line string) {
	reflectR := reflect.ValueOf(r).Elem()
	content := strings.TrimSpace(line[len(prefix)+1:])
	reflectR.FieldByName(field).SetString(content)
}

// parse cookie
func parseCookie(r *Request, prefix, field, line string) {
	r.Cookie = make(map[string]string)
	cookies := strings.TrimSpace(line[len(prefix)+1:])
	cookieList := strings.Split(cookies, ";")

	for _, item := range cookieList {
		item = strings.TrimSpace(item)
		splitItem := strings.Split(item, "=")
		r.Cookie[splitItem[0]] = splitItem[1]
	}
}

// parse http request
func parseRequest(reqSlice []string) *Request {
	req := &Request{}

	for _, line := range reqSlice {
		for prefix, procMeta := range parseMeta {
			if strings.HasPrefix(line, prefix) {
				procMeta.Proc(req, prefix, procMeta.Field, line)
			}
		}
	}
	return req
}

// logger client request info
func loggerReqInfo(conn net.Conn, req *Request) {
	clientIp := conn.RemoteAddr().String()
	reqURL := req.PathInfo
	clientUserAgent := req.UserAgent
	logger.Logger.Info(
		"Ip: %s request for %s user agent %s",
		clientIp,
		reqURL,
		clientUserAgent,
	)
}

// return http request channels
func RequestsChans(conn net.Conn) chan *Request {
	reader := bufio.NewReader(conn)
	reqCap := config.SerConfig.ReqChanCap
	reqChans := make(chan *Request, reqCap)

	go func() {
		reqSlice := make([]string, reqCap)
		for {
			line, err := reader.ReadString('\n')
			if err == nil {
				line = strings.TrimSpace(line)
				reqSlice = append(reqSlice, line)
				if len(line) == 0 {
					req := parseRequest(reqSlice)
					loggerReqInfo(conn, req)
					reqChans <- req
					reqSlice = reqSlice[:0]
				}
			} else {
				logger.Logger.Debug(
					"Ip %s connection close, close msg %s",
					conn.RemoteAddr().String(),
					err.Error(),
				)
				conn.Close()
				break
			}
		}
		close(reqChans)
	}()
	return reqChans
}
